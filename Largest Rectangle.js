'use strict';

const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
  inputString += inputStdin;
});

process.stdin.on('end', _ => {
  inputString = inputString.replace(/\s*$/, '')
    .split('\n')
    .map(str => str.replace(/\s*$/, ''));

  main();
});

function readLine() {
  return inputString[currentLine++];
}

// Complete the largestRectangle function below.
function largestRectangle(h) {

  // let answer = 0;
  // const n = h.length;
  // for (let c = 0; c < n; c++) {
  //   let l = c, r = c;
  //   while (l >= 0 && h[l] >= h[c]) {
  //     l--;
  //   }
  //   while (r <= n && h[r] >= h[c]) {
  //     r++;
  //   }
  //   answer = Math.max((r - (l + 1)) * h[c], answer);
  // }
  // return answer;

  let max = 0;
  for (let i = 0; i < h.length; i++) {
    let count = 1;
    for (let x = (i + 1); x < h.length; x++) {
      if (h[i] <= h[x]) {
        count++;
      }
      else {
        break;
      }
    }
    for (let y = (i - 1); y >= 0; y--) {
      if (h[i] <= h[y]) {
        count++;
      }
      else {
        break;
      }
    }
    if (h[i] * count > max) {
      max = h[i] * count;
    }
  }
  return max;

}



function main() {
  const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

  const n = parseInt(readLine(), 10);

  const h = readLine().split(' ').map(hTemp => parseInt(hTemp, 10));

  let result = largestRectangle(h);

  ws.write(result + "\n");

  ws.end();
}
