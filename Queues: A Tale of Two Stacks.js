function processData(input) {
  //Enter your code here


  // let queue = [];
  // input = input.match(/.+\b/g);

  // for (let i = 1; i < input.length; i++) {
  //   let re = /(\d+)\s(\d+)\b/.exec(input[i]);
  //   if (re) {
  //     queue.push(re[2]);
  //   } else if (input[i] == 2) {
  //     queue.shift();
  //   } else {
  //     console.log(queue[0]);
  //   }
  // }


  // const answer = [];
  // const lines = input.split('\n').slice(1);
  // const s = [];
  // const peek = () => s[0];
  // for (const line of lines) {
  //   const [command, value] = line.split(' ');
  //   switch (command) {
  //     case '1':
  //       s.push(parseInt(value, 10));
  //       break;
  //     case '2':
  //       s.shift();
  //       break;
  //     case '3':
  //       answer.push(peek());
  //   }
  // }
  // console.log(answer.join('\n'));


  // let result = [];
  // let inputArr = input.split("\n");
  // for (let i = 1; i < inputArr.length; i++) {
  //   switch (inputArr[i][0]) {
  //     case '1':
  //       let value = inputArr[i].slice(1).trim();
  //       result.push(value);
  //       break;
  //     case '2':
  //       if (result.length != 0) {
  //         result.shift();
  //       }
  //       break;
  //     case '3':
  //       if (result.length != 0) {
  //         console.log(result[0]);
  //       }
  //       break;
  //   }
  // }


  const queries = input.split('\n').map(query => query.split(' '));
  const queue = [];
  queries.shift();
  for (let [type, value] of queries) {
    if (type === '1') {
      queue.unshift(value);
    } else if (type === '2') {
      queue.pop();
    } else {
      console.log(queue[queue.length - 1]);
    }
  }

}

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
  _input += input;
});

process.stdin.on("end", function () {
  processData(_input);
});
