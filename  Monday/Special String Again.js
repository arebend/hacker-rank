'use strict';

const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
  inputString += inputStdin;
});

process.stdin.on('end', function () {
  inputString = inputString.replace(/\s*$/, '')
    .split('\n')
    .map(str => str.replace(/\s*$/, ''));

  main();
});

function readLine() {
  return inputString[currentLine++];
}

// Complete the substrCount function below.
function substrCount(n, s) {

  let counter = 0;
  // loop over the string
  for (let i = 0; i < n; i++) {
    // expend on odd palindrome
    counter += expendSpecial(s, i, i);
    // expend on even palindrome
    counter += expendSpecial(s, i, i + 1);
  }


  function expendSpecial(s, start, end) {
    let counter = 0;
    // expend from passed caracters until ends match
    while (start >= 0 && end < n && s[start] === s[end]) {
      const subStr = s.substring(start, end + 1);
      // if substring is special, increased the counter
      if (isSpecialString(subStr)) counter++;
      // expend more
      start--;
      end++;
    }
    return counter;
  }


  function isSpecialString(s) {
    // interate over first half of the string
    // stop before the middle character
    for (let i = 0; i < Math.floor(s.length / 2); i++) {
      // check if the end matches
      if (s[i] !== s[s.length - 1 - i]) {
        return false;
        // check if next char matches prev
      } else if (i > 0 && s[i] !== s[i - 1]) {
        return false;
      }
    }
    return true;
  }
  return counter
}

function main() {
  const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

  const n = parseInt(readLine(), 10);

  const s = readLine();

  const result = substrCount(n, s);

  ws.write(result + '\n');

  ws.end();
}
