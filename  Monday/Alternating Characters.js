'use strict';

const fs = require('fs');
const { log } = require('console');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
  inputString += inputStdin;
});

process.stdin.on('end', _ => {
  inputString = inputString.replace(/\s*$/, '')
    .split('\n')
    .map(str => str.replace(/\s*$/, ''));

  main();
});

function readLine() {
  return inputString[currentLine++];
}

// Complete the alternatingCharacters function below.
function alternatingCharacters(s) {

  let last = s.charAt(0);
  // let last = s[0]
  // console.log(last);
  let result = 0;

  for (let i = 1; i < s.length; i++) {
    // console.log(s.charAt(i));
    if (last === s.charAt(i)) {
      result++;
    } else {
      last = s.charAt(i);
    }
  }
  return result;
}

console.log(alternatingCharacters('AAAA')); // 3 deletions
console.log(alternatingCharacters('BBBBB')); // 4 deletions
console.log(alternatingCharacters('ABABABAB')); // 0 deletion
console.log(alternatingCharacters('BABABA')); // 0 deletion
console.log(alternatingCharacters('AAABBB')); // 4 deletions


function main() {
  const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

  const q = parseInt(readLine(), 10);

  for (let qItr = 0; qItr < q; qItr++) {
    const s = readLine();

    let result = alternatingCharacters(s);

    ws.write(result + "\n");
  }

  ws.end();
}
